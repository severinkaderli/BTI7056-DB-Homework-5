---
title: "BTI7056-DB"
subtitle: "Homework 5"
author:
    - Severin Kaderli
    - Marius Schär
extra-info: true
institute: "Berner Fachhochschule"
department: "Technik und Informatik"
lecturer: "Dr. Kai Brünnler"
lang: "de-CH"
toc: false
header-includes: |
    \usepackage[]{algorithm2e}
    \usepackage{setspace}
    \doublespacing
    \renewcommand{\arraystretch}{0.7}
    \def\code#1{\texttt{#1}}
...

# Aufgabe 1
Gegeben sind folgende Relationen $r$ und $s$:

: Table $r(A,B,C,D)$

| A    | B    | C    | D       |
| ---- | ---- | ---- | ------- |
| "A"  | 1000 | 3    | ""      |
| "A"  | 700  | NULL | "agh"   |
| "A"  | NULL | 0    | "abcdf" |
| "A"  | 1000 | 4    | NULL    |
| "B"  | NULL | NULL | "bdf"   |
| "B"  | 1500 | NULL | "c"     |
| NULL | 1000 | 8    | ""      |
| NULL | 700  | 12   | NULL    |

: Table $s(A,E)$

| A   | E   |
| --- | --- |
| "B" | 1   |
| "C" | 2   |
| "C" | 3   |

## Evaluieren Sie für jede Zeile von $r$ den Wert des Prädikats $p$ mit:
$$
p = (B \cdot C < 5000 \lor D \code{ is null})
$$

| A    | B    | C    | D       | $p$      |
| ---- | ---- | ---- | ------- | ------   |
| "A"  | 1000 | 3    | ""      | `true`   |
| "A"  | 700  | NULL | "agh"   | `unknown`|
| "A"  | NULL | 0    | "abcdf" | `unknown`|
| "A"  | 1000 | 4    | NULL    | `true`   |
| "B"  | NULL | NULL | "bdf"   | `unknown`|
| "B"  | 1500 | NULL | "c"     | `unknown`|
| NULL | 1000 | 8    | ""      | `false`  |
| NULL | 700  | 12   | NULL    | `true`   |

## $\sigma_{p} (r)$
| A    | B    | C    | D       |
| ---- | ---- | ---- | ------- |
| "A"  | 1000 | 3    | ""      |
| "A"  | 1000 | 4    | NULL    |
| NULL | 700  | 12   | NULL    |

## $A \mathcal{G}_{\code{avg}(B), \code{sum}(C)} (r)$
| A    | avg(B)   | sum(C)  |
| ---- | --- | -- |
| "A"  | 983 | 27 |
| "B"  | 983 | 27 |
| NULL | 983 | 27 |

## $A \mathcal{G}_{\code{avg}(B)} (\Pi_{A,B} (r))$
| A    | B   |
| ---- | --- |
| "A"  | 983 |
| "B"  | 983 |
| NULL | 983 |

## $r \naturaljoin s$
| A    | B    | C    | D       | E |
| ---- | ---- | ---- | ------- | - |
| "B"  | NULL | NULL | "bdf"   | 1 |
| "B"  | 1500 | NULL | "c"     | 1 |

## $r \rightouterjoin s$
| A     | B    | C    | D       | E |
| ----  | ---- | ---- | ------- | - |
| "B"   | NULL | NULL | "bdf"   | 1 |
| "B"   | 1500 | NULL | "c"     | 1 |
| NULL  | NULL | NULL | NULL    | 2 |
| NULL  | NULL | NULL | NULL    | 3 |

## $r \fullouterjoin s$
| A    | B    | C    | D       | E    |
| ---- | ---- | ---- | ------- | -    |
| "A"  | 1000 | 3    | ""      | NULL |
| "A"  | 700  | NULL | "agh"   | NULL |
| "A"  | NULL | 0    | "abcdf" | NULL |
| "A"  | 1000 | 4    | NULL    | NULL |
| "B"  | NULL | NULL | "bdf"   | 1    |
| "B"  | 1500 | NULL | "c"     | 1    |
| NULL | 1000 | 8    | ""      | NULL |
| NULL | 700  | 12   | NULL    | NULL |
| NULL | NULL | NULL | NULL    | 2    |
| NULL | NULL | NULL | NULL    | 3    |

\newpage

# Aufgabe 2
> Drücken Sie die gegebenen Abfragen für das gegebene DB-Schema in der Sprache
> der Relationalen Algebra aus.
> 
> $\text{employee(} \underline{\text{person\_name}} \text{, street, city)}$  
> $\text{works(} \underline{\text{person\_name}} \text{, company\_name, salary)}$  
> $\text{company(} \underline{\text{company\_name}} \text{, city)}$  
> $\text{manages(} \underline{\text{person\_name}} \text{, manager\_name)}$  

## Finde die Firma mit den meisten Angestellten.
$$
\Pi_{\text{company\_name}} (
  \text{company\_name} \mathcal{G}_{\code{max}(\text{count(person\_name)})}(
    \text{company\_name} \mathcal{G}_{\code{count}(\text{person\_name})}(\text{works})
  )
)
$$

## Finde die Firma, welche insgesamt die kleinste Lohnsumme bezahlt.
$$
\Pi_{\text{company\_name}} (
  \text{company\_name} \mathcal{G}_{\code{min}(\text{sum(salary)})}(
    \text{company\_name} \mathcal{G}_{\code{sum}(\text{salary})}(\text{works})
  )
)
$$

## Finde diejenigen Firmen, deren Angestellte im Durchschnitt mehr verdienen, als der Durchschnittslohn der "FBC".
$$
\text{avg\_fbc\_salary} \leftarrow \mathcal{G} \code{avg}(\text{salary}) (
  \sigma_{\text{company\_name} = 'FBC'} (
      \text{works}
  )
)
$$
$$
\Pi_{\text{company\_name}} (
  \sigma_{\text{avg(salary)} > \text{avg\_fbc\_salary}} (
    \text{company\_name} \mathcal{G} \code{avg}(\text{salary}) (
      \text{works}
    )
  )
)
$$

\newpage

# Aufgabe 3
> Rufen Sie sich folgende Konzepte wieder in Erinnerung:
> 
> - den Algorithmus für binäre Suche
> - die Zeitkomplexität eines Algorithmus
> 
> Gegeben seien Relationen $r(A,B)$ und $s(B,C)$, als Listen von Tupeln.
> Attribut $B$ ist vom Typ `Integer`. Gehen Sie von dem in der letzten Übung entwickelten
> Algorithmus für den natürlichen Join aus, und verfeinern Sie ihn für folgende Fälle in einen
> *möglichst effizienten* Algorithmus, um den natural join der beiden Relationen zu berechnen.
> Geben sie jewils die Zeitkomplexität des Algorithmus an.
>
> Bei einem Tupel `t` können Sie mit `t['B']` auf den Wert des Attributs $B$ zugreifen.

Code der letzten Übung, $\mathcal{O}(n^2)$

```{.pseudocode .numberLines}
list natJoin (list r, list s) :
  result := nil;
  foreach rTuple in r :
    rB := head(tail(rTuple));
    foreach sTuple in s :
      sB := head(sTuple);
      if rB = sB :
        result := const(join(sTuple, rTuple), result);
  return result;

list join(list sTuple, list rTuple):
  resultTuple := nil;
  resultTuple := const(head(tail(sTuple)), resultTuple);
  resultTuple := const(rB, resultTuple);
  resultTuple := const(head(rTuple), resultTuple);
  return resultTuple;
```
\newpage
## $\{B\}$ ist ein Schlüsselkandidat für $r$,

HashJoin ist eine viel effizientere Weise einen Join durchzuführen:

Zeitkomplexität von $\mathcal{O}(n + m)$ wobei $n = \code{len}(s), m = \code{len}(r)$.  
Spacekomplexität von $\mathcal{O}(n)$ wobei $n = \code{len}(s)$.

Unter Annahme das der Zugriff auf `hashTable` in $\mathcal{O}(1)$ erfolgt.

```{.pseudocode .numberLines}
list natHashJoin (list r, list s):
  hashTable := nil;
  result := nil;

  foreach sTuple in s:
    h := hash(sTuple['B']);
    hashTuple := const(hash, sTuple);
    hashTable := const(hashTuple, hashTable);
  
  for rTuple in r:
    h := hash(rTuple['B']);
    sTuple := lookup(hashTable, h);
    if (sTuple is not null and sTuple['B'] = rTuple['B']):
        result := const(join(es, rs), result);

  return result;
      
list join(list sTuple, list rTuple):
  resultTuple := nil;
  resultTuple := const(head(tail(sTuple)), resultTuple);
  resultTuple := const(rB, resultTuple);
  resultTuple := const(head(rTuple), resultTuple);
  return resultTuple;
```

## wie 3.1 und zusätzlich ist die Liste für $r$ nach Attribut $B$ sortiert,
s. 3.1

## wie 3.2 und zusätzlich ist die Liste für $s$ nach Attribut $B$ sortiert.
s. 3.1
